package com.example.homework02;

public class ProfileDetails {

    private String image, webTitle, webCategory, webName, email, contentCategory, radioFacebook, radioWebsite, radioOption;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWebTitle() {
        return webTitle;
    }

    public void setWebTitle(String webTitle) {
        this.webTitle = webTitle;
    }

    public String getWebCategory() {
        return webCategory;
    }

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }

    public void setWebCategory(String webCategory) {
        this.webCategory = webCategory;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContentCategory() {
        return contentCategory;
    }

    public void setContentCategory(String contentCategory) {
        this.contentCategory = contentCategory;
    }

    public String getRadioFacebook() {
        return radioFacebook;
    }

    public void setRadioFacebook(String radioFacebook) {
        this.radioFacebook = radioFacebook;
    }

    public String getRadioWebsite() {
        return radioWebsite;
    }

    public void setRadioWebsite(String radioWebsite) {
        this.radioWebsite = radioWebsite;
    }

    public String getRadioOption() {
        return radioOption;
    }

    public void setRadioOption(String radioOption) {
        this.radioOption = radioOption;
    }
}
