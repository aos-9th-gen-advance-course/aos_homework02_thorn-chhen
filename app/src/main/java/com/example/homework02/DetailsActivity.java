package com.example.homework02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {

    // declaration variable for text view and button
    TextView get_title, get_web_name, get_email, radio_option, position;
    Button btn_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // register button from activity detail xml
        btn_back = findViewById(R.id.buttonBack);

        // register text view from activity detail xml
        get_title = findViewById(R.id.getTitleWebsite);
        get_web_name = findViewById(R.id.getNameWeb);
        get_email = findViewById(R.id.getEmail);
        radio_option = findViewById(R.id.getRadioCategories);
        position = findViewById(R.id.getContentCategories);

        Intent intent = getIntent();

        // get value from main activity
        String title = intent.getStringExtra("title");
        String web_name = intent.getStringExtra("webName");
        String email = intent.getStringExtra("email");
        String option = intent.getStringExtra("radioOption");
        String sPosition = intent.getStringExtra("position");


        // set value to object
        get_title.setText(title);
        get_web_name.setText(web_name);
        get_email.setText(email);
        radio_option.setText(option);
        position.setText(sPosition);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(i);
            }
        });


    }

}