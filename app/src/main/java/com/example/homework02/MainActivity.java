package com.example.homework02;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    String[] position = {"Web Developer", "iOS", "AOS", "Spring", "DevOp"};

    Button onSelectImage, onButtonSave, onButtonClear;
    ImageView imageProfile;
    EditText etTitle, etWebName, etEmail;
    Spinner etContentCategories;
    RadioGroup radioGroup;
    RadioButton radioWeb, radioFb;

    ProfileDetails pd = new ProfileDetails();

    ImageView onViewImage;
    RadioButton radioButton;

    // constant to compare
    // the activity result code
    int SELECT_PICTURE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        onSelectImage = findViewById(R.id.imgButtonChooser);
        onViewImage = findViewById(R.id.imageProfileView);
        onButtonSave = findViewById(R.id.onButtonSave);
        onButtonClear = findViewById(R.id.onButtonClear);

        radioGroup = findViewById(R.id.radioGroupCategories);
        etTitle = findViewById(R.id.TitleTextField);
        radioWeb = findViewById(R.id.radio_button_website);
        radioFb = findViewById(R.id.radio_button_facebook);
        etTitle = findViewById(R.id.TitleTextField);
        etEmail = findViewById(R.id.WebNameTextField);
        etWebName = findViewById(R.id.TextFieldWebName);
        etContentCategories = findViewById(R.id.chooseCategoriesSpinner);

        Spinner spinner = findViewById(R.id.chooseCategoriesSpinner);

        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.spinner_choose_categories, android.R.layout.simple_spinner_dropdown_item);

        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        // Handle button choose image profile
        onSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onImageChoose();
            }
        });


        // Handle button save information to information details activity
        onButtonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);


                pd.setWebTitle(etTitle.getText().toString());
                pd.setWebName(etWebName.getText().toString());
                pd.setEmail(etEmail.getText().toString());
                pd.setRadioOption(radioButton.getText().toString());
                pd.setContentCategory(etContentCategories.toString());


                Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);

                intent.putExtra("title", pd.getWebTitle());
                intent.putExtra("webName", pd.getWebName());
                intent.putExtra("email", pd.getEmail());
                intent.putExtra("radioOption", pd.getRadioOption());
                intent.putExtra("position", pd.getContentCategory());

                startActivity(intent);
            }
        });

    }

    // this function is triggered when
    // the Select Image Button is clicked
    private void onImageChoose() {

        // create an instance of the
        // intent of the type image
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        // pass the constant to compare it
        // with the returned requestCode
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            // compare the resultCode with the
            // SELECT_PICTURE constant
            if (requestCode == SELECT_PICTURE) {
                // Get the url of the image from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // update the preview image in the layout
                    onViewImage.setImageURI(selectedImageUri);
                }
            }
        }
    }
}